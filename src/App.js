
// CSS Stylesheet
import './styles/App.css';

// Bootstrap
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

//NPM 
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { UserProvider } from './userContext';
import { TagManager }   from 'react-gtm-module';

// Pages 
import About from './pages/About';
import Admin from './pages/Admin';
import AddProduct from './pages/AddProduct';
import AppNavbar from './pages/AppNavbar';
import Home from './pages/Home';
import Footer from './pages/Footer';
import ForgotPassword from './pages/ForgotPassword';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import Profile from './pages/Profile';
import ProductDetail from './pages/ProductDetail';
import Products from './pages/Products';
import Signup from './pages/Signup';
import Cart from './pages/Cart';

export default function App(){

  const [ user, setUser ] = useState({
      id: null,
      firstname: null,
      lastname: null,
      isAdmin: null
  });

  const [counter, setCounter] = useState(0)
  console.log(user)

  const unsetUser = () => {
    localStorage.clear()    
  }

  // const tagManagerArgs = {
  //   gtmId: 'GTM-TLZXXTF'
  // }

  // TagManager.initialize(tagManagerArgs)

  useEffect(() => {
    let token = localStorage.getItem('token');
    console.log(token);
    fetch('https://safe-bastion-71965.herokuapp.com/user/profile',{
      method: 'GET',
      headers: { Authorization: `Bearer ${token}`}
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          firstname: data.fullName[0].givenName,
          lastname: data.fullName[0].familyName,
          isAdmin: data.isAdmin
        })
      }else setUser({id: null, isAdmin: null})
    })
  }, []);

  return <UserProvider value={{user, setUser, unsetUser, counter,setCounter}}>
  <Router>
  	<AppNavbar/>
  	<Routes>
  		<Route exact path="/" element={<Home/>}/>
  		<Route exact path="/about" element={<About/>}/>
  		<Route exact path="/products" element={<Products/>}/>
  		<Route exact path="/signup" element={<Signup/>}/>
  		<Route exact path="/login" element={<Login/>}/>
      <Route exact path="/logout" element={<Logout/>}/>
      <Route exact path="/profile" element={<Profile/>}/>
      <Route exact path="/product-detail/:productId" element={<ProductDetail/>}/>
      <Route exact path="/addProduct" element={<AddProduct/>}/>
      <Route exact path="/forgot-password" element={<ForgotPassword/>}/>
      <Route exact path="/cart" element={<Cart/>}/>
      <Route exact path="/admin" element={<Admin/>}/>
      <Route path="*" element={<PageNotFound/>}/>
  	</Routes>
  	<Footer/>
  </Router>
  </UserProvider>
}

