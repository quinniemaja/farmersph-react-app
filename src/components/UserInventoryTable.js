
import {useState, useEffect, useContext, Fragment} from 'react'
import {useNavigate} from 'react-router-dom'
import {Table, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

import '../styles/UserInventory.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faToggleOn } from '@fortawesome/free-solid-svg-icons'; 
import { faToggleOff  } from '@fortawesome/free-solid-svg-icons'; 


export default function UserInventory ({userProp, pageReload}) {
	const {counter, setCounter} = useContext(UserContext);
	const history = useNavigate();


	const {email, isAdmin, isActive, _id} = userProp
	const [type, setType] = useState('');
	const [status, setStatus] = useState('');
	const [buttonStat, setButtonStat] = useState('');
	const [adminCheck, setAdminCheck] = useState(isAdmin);
	

	// console.log(isAdmin);


	async function setUpAdmin() {
    		const response = await fetch(`https://safe-bastion-71965.herokuapp.com/user/assign-admin-status/${_id}`, {
    			method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
					},
				body: JSON.stringify({
					isAdmin: true
				}),
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					if(data.isActive){
						Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'This user is now Admin'
						});
					pageReload(counter +1);
					setType('Admin')
					history('/admin');
					} else {
					Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
	
	  		})
    	}

    	async function unSetAdmin() {
    		const response = await fetch(`https://safe-bastion-71965.herokuapp.com/user/assign-admin-status/${_id}`, {
    			method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
					},
				body: JSON.stringify({
					isAdmin: false
				}),
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					if(data){
						Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'User status has been updated!'
						});
					pageReload(counter +1);
					setType('User');
					history('/admin');
					} else {
					Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
	
	  		})
    	}


   useEffect(() => {
    	async function changeType() {

    		const response = await fetch(`https://safe-bastion-71965.herokuapp.com/user/profile/${_id}`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}) 
    	.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data.isAdmin && data.isActive){
      		setType('Admin');
      		setStatus('Active');
      		} else if(data.isActive) {
      			setType('User');
      			setStatus('Active')
      		}else {
      			setType('User');
      			setStatus('Inactive');
      		}
 		 })
			
    	}
    	setCounter(counter +1);
    	changeType(); 	





    }, [])


	return (

		
		    <tr className = 'User_item_row'>
		      <td className = 'user_item_name'>{email}</td>
		      <td className = 'user_item_name'>{type}</td>
		      <td className = 'user_item_name'>{status}</td>
		      <td>
		      		{(type == 'Admin') ?
		      		<Fragment>
					<FontAwesomeIcon className="active_icon fa-2x mx-3" type ='submit' onClick={unSetAdmin} icon={faToggleOff}/>
		      		</Fragment>
		      		:
		      		<Fragment>
		      		<FontAwesomeIcon className="inactive_icon fa-2x mx-3" type ='submit' onClick={setUpAdmin} icon={faToggleOn}/>
		      			

					</Fragment>}
		      </td>
		    </tr>
		 
		

	)
}