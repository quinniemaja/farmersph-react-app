import '../styles/GeneralInformation.css';
import { Fragment, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';
export default function GeneralInformation({nextStep, handleChange, values}){

	const { user } = useContext(UserContext);

	let {firstname, lastname, gender, birthday, lotNumber, barangay, city, country} = values
	let disableButton = true;
	const Continue = (e) => {
		e.preventDefault();
		nextStep();
	}

	if(firstname && lastname && gender && birthday && lotNumber && barangay && city && country){
		disableButton = false
	}else disableButton = true


	return <Fragment>
		{user.id? <Navigate to='/profile'/>:
		<Form>
		<div className="signup-general-information">
		<p className="sign-up__header">General Information</p>
		 
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label className="form-label-header">First Name</Form.Label>
			    <Form.Control 
			    	onChange={handleChange} 
			    	size="sm" 
			    	type="text"
			    	name="firstname"
			    	value= {values.firstname}
			    	placeholder="Please enter your first name" />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label className="form-label-header">Last Name</Form.Label>
			    <Form.Control 
			    	onChange={handleChange} 
			    	size="sm" 
			    	type="text"
			    	name="lastname" 
			    	value= {values.lastname}
			    	placeholder="Please enter your last name" />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label className="form-label-header">Address</Form.Label>
				    <div className="signup-address-input">
				    <Form.Control  
				    	onChange={handleChange}  
				    	className="lotNumber" 
				    	size="sm" 
				    	type="text" 
				    	name="lotNumber"
			    		value= {values.lotNumber}
				    	placeholder="Lot number" />
				    <Form.Control  
				    	onChange={handleChange} 
				    	className="barangay" 
				    	size="sm" 
				    	type="text" 
				    	name="barangay"
			    		value= {values.barangay}
				    	placeholder="Barangay" />
			    </div>
			    <div className="signup-country">
				    <Form.Control  
				    	onChange={handleChange} 
				    	size="sm" 
				    	type="text" 
				    	name="city"
			    		value= {values.city}
				    	placeholder="City" />
				    <Form.Control  
				    	onChange={handleChange}  
				    	size="sm" 
				    	type="text" 
				    	name="province"
			    		value= {values.province}
				    	placeholder="Province" />
				    <Form.Control  
				    	onChange={handleChange}  
				    	size="sm" 
				    	type="text" 
				    	name="country"
			    		value= {values.country}
				    	placeholder="Country" />
			    </div>
			  </Form.Group>
				<Form.Label className="form-label-header">Sex</Form.Label>		  
			  {['radio'].map((type) => (
			    <div key={`inline-${type}`} className="mb-3">
			      <Form.Check
			        inline
			        onChange={handleChange}
			        label="Male"
			        value="Male"
			        name="gender"
			        type={type}
			        id={`inline-${type}-1`}
			        checked={'Male' === gender}
			      />
			      <Form.Check
			        inline
			        onChange={handleChange}
			        value="Female"
			        label="Female"
			        name="gender"
			        type={type}
			        id={`inline-${type}-2`}
			        checked={'Female' === gender}
			      />
			    </div>))}
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label className="form-label-header">Birthday</Form.Label>
			    <Form.Control 
			    	onChange={handleChange} 
			    	size="sm" 
			    	name="birthday"
			    	type="date" 
			    	value= {values.birthday}
			    	placeholder="Please enter your birthday" />
			  </Form.Group>	  
		</div>
		<Button className="next-btn" onClick={ Continue } disabled={disableButton? true: false}>Next</Button>	
		</Form>}
	</Fragment>
}