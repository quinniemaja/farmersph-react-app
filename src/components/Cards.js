import '../styles/Card.css';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Cards(props){
	console.log(props.image);
	return <Card key={props.id}  className="card">
	  {<Card.Img variant="top" src={props.image} className="card-img" />}
	  <Card.Body>
	  	<div className="card-product__header">
	    		<Card.Title className="card-body__title">{props.title}</Card.Title>
	    		<p className="card-body__price">₱{props.price}</p>
	    </div>
	    <small className="card-body__total-rates">(260)</small>
	    <Button variant="primary" size="sm" className="card-body__btn" as={Link} to={`/product-detail/${props.id}`} exact="true">details</Button>
	  </Card.Body>
	</Card>
}