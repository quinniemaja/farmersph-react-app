import {Fragment, useState, useEffect, useContext} from 'react'
import Table from '../components/OrderTable'
import UserContext from '../userContext';

export default function Orders() {

	const {counter, setCounter} = useContext(UserContext); 
	const[product, setProduct] = useState([]);



	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setProduct(data.sort().reverse().map(item => {
				return (
					<Table keyOrder = {item.id} ordered = {item}/>
					)
			}))
		})
	}, [])
	// console.log(product);


	return(

		<Fragment>
			{product}
		</Fragment>



	)
}