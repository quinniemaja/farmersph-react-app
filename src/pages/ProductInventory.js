
import {Fragment, useState, useEffect, useContext} from 'react';
import Table from '../components/ProductInventoryTable'
import UserContext from '../userContext';

export default function ProductInventory () {
	const {counter, setCounter} = useContext(UserContext); 
	const [item, setItem] = useState([]);

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product',{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(item => {
			// console.log(item)

			setItem(item.sort().reverse().map(prod => {
				return (
					<Table key = {prod.id} product = {prod} reload={setCounter}/>
					

				)
			}))
		})
	}, [counter])




	return (
		<Fragment>
			{item}
		</Fragment>
	)
}