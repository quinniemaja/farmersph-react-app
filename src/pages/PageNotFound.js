import Container from '../components/Container';
export default function PageNotFound(){
	return <Container>	
		<h1>404 Page Not Found</h1>
	</Container>
}