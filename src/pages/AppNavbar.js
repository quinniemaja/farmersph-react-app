import '../styles/AppNavbar.css';
import { Fragment, default as React, useContext, useState, useEffect } from 'react';
import { Navbar,Nav, Form, Button, FormControl, NavDropdown, Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';
// Fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faSearch  } from '@fortawesome/free-solid-svg-icons'; 
/*import { faFacebookF, faInstagram, faTwitter, faLinkedinIn } from "@fortawesome/free-brands-svg-icons"*/

export default function AppNavbar(){

	const {user, counter} = useContext(UserContext);
	const {firstname, lastname} = user;
	const userId = user.id
	const token = localStorage.getItem('token')
	let [itemsLength, setItemsLength] = useState();

	console.log(itemsLength)
	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.auth !== 'failed'){
				const items = data.filter(item => item.userId === userId)
				setItemsLength(items.length)
				console.log(items.length)				
			}
		});		
	}, [counter])
	console.log(user)
	return <Fragment> 
		<Navbar fixed="top" expand="md" className="navbar">
		  <Navbar.Brand as={Link} to="/" exact="true" className="navbar-brand">Farmers<span className="navbar-brand__country">PH</span>.</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">

		    </Nav>
		    <Form inline="true" className="form">
		      <FormControl type="text" placeholder="Search" size="sm" className="form-control"/>
		      <Button size="sm" className="form-button__search"><FontAwesomeIcon icon={faSearch}/></Button>
		    </Form>
		    <Nav className="ml-auto">
		    {(user.id !== null)? <Fragment>
		     		<Nav.Link as={Link} to="/products" exact="true" className="navbar-link">Products</Nav.Link>
			        <NavDropdown title={<span  className="navbar-link">{firstname +' '+ lastname}</span>} id="basic-nav-dropdown">
			          <NavDropdown.Item as={Link} to="/profile" exact="true"  className="navbar-link">Manage Profile</NavDropdown.Item>
			          {(user.isAdmin !== false)?
			          	<Fragment>
			          	<NavDropdown.Item href="/admin" className="navbar-link">Admin</NavDropdown.Item>
			          	</Fragment>
			          	: <Fragment>  	
			          <NavDropdown.Item as={Link} to="/cart" exact="true"  className="navbar-link">My Cart
			          	{(itemsLength > 0)? <Badge variant="danger" className="badge-icon">{itemsLength}</Badge>:null} 
			          </NavDropdown.Item>
			      		</Fragment>}
			        </NavDropdown>		    	
		      		<Nav.Link as={Link} to="/logout" exact="true" className="navbar-link">Logout</Nav.Link>
			   </Fragment>	
		      :<Fragment>
		     		<Nav.Link as={Link} to="/products" exact="true" className="navbar-link">Products</Nav.Link>
		     		<Nav.Link as={Link} to="/signup" exact="true" className="navbar-link">Sign up</Nav.Link>
		      		<Nav.Link as={Link} to="/login" exact="true" className="navbar-link">Login</Nav.Link>
		       </Fragment>
		    }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	</Fragment>
}

